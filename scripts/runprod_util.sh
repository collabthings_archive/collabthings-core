#!/bin/bash

jarfile=$(ls target/*-dependencies.jar)
java -DWAAZDOH_RUNNER=true -Xmx2048m -Dservice.url=http://collabthings.org:8088/waazdoh -cp ${jarfile} waazdoh.client.app.AppLauncher prod 
