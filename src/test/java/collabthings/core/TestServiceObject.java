package collabthings.core;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import org.collabthings.core.ServiceObject;
import org.collabthings.core.ServiceObjectData;
import org.collabthings.core.ServiceObjectListener;
import org.collabthings.core.WClient;
import org.collabthings.core.WClient.Filter;
import org.collabthings.core.utils.WTimedFlag;
import org.collabthings.datamodel.WObject;
import org.collabthings.datamodel.WObjectID;
import org.collabthings.datamodel.WaazdohInfo;
import org.xml.sax.SAXException;

public class TestServiceObject extends WCTestCase {

	private final class ServiceObjectDataImplementation implements ServiceObjectData {
		double d = Math.random();
		long time = System.currentTimeMillis();

		@Override
		public boolean parse(WObject bean) {
			d = bean.getDoubleValue("d");
			time = bean.getLongValue("time");
			return true;
		}

		@Override
		public WObject getObject() {
			WObject b = o.getBean();
			b.addValue("d", d);
			b.addValue("time", time);
			return b;
		}

		public void update() {
			d = Math.random();
		}
	}

	ServiceObject o;

	public void testModifiedListener() throws MalformedURLException, SAXException {
		ServiceObjectData data1 = new ServiceObjectDataImplementation();

		WClient c1 = getClient(false);
		o = new ServiceObject("test", c1, data1, WaazdohInfo.VERSION, "WAAZDOHTEST");

		final Map<String, String> map = new HashMap<String, String>();

		o.addListener(new ServiceObjectListener() {

			@Override
			public void modified() {
				map.put("done", "true");
			}
		});

		o.save();
		o.publish();

		assertEquals(map.get("done"), "true");
	}

	public void testServiceObject() throws SAXException, MalformedURLException {
		ServiceObjectData data1 = new ServiceObjectDataImplementation();

		WObjectID id = createAndPublish(data1);
		assertNotNull(id);

		ServiceObjectDataImplementation data2 = new ServiceObjectDataImplementation();

		WClient c2 = getClient(false);
		ServiceObject o1 = new ServiceObject("test", c2, data2, WaazdohInfo.VERSION, "WAAZDOHTEST");

		assertTrue(o1.load(id.getStringID()));

		WObject o2data = data2.getObject();

		WObject o1data = data1.getObject();

		assertEquals(o1data, o2data);
		assertEquals(o1data.getContentHash(), o2data.getContentHash());

		String o1hash = o1.getHash();
		data2.update();
		assertTrue(o1.hasChanged());

		o1.save();
		assertNotNull(o1.getCopyOf());
		assertFalse(o1.getHash().equals(o1hash));
	}

	public void testFilter() throws SAXException, MalformedURLException {
		ServiceObjectData data1 = new ServiceObjectDataImplementation();

		WObjectID id = createAndPublish(data1);
		assertNotNull(id);

		ServiceObjectDataImplementation data2 = new ServiceObjectDataImplementation();

		WClient c2 = getClient(false);
		ServiceObject o1 = new ServiceObject("test", c2, data2, WaazdohInfo.VERSION, "WAAZDOHTEST");

		final WTimedFlag f = new WTimedFlag(getWaitTime());

		c2.addObjectFilter(new Filter() {
			int counter = 0;

			@Override
			public boolean check(WObject o) {
				f.trigger();
				return counter++ > 0;
			}
		});

		o1.load(id.getStringID());
		assertTrue(f.isTriggered());
		assertTrue(!data1.getObject().equals(data2.getObject()));

		o1.load(id.getStringID());
		assertTrue(f.isTriggered());

		WObject o2data = data2.getObject();

		WObject o1data = data1.getObject();

		assertTrue(data1.getObject().equals(data2.getObject()));
		assertEquals(o1data.getContentHash(), o2data.getContentHash());
	}

	private WObjectID createAndPublish(ServiceObjectData data1) throws MalformedURLException, SAXException {
		WClient c1 = getClient(false);
		o = new ServiceObject("test", c1, data1, WaazdohInfo.VERSION, "WAAZDOHTEST");

		o.save();
		o.publish();

		WObjectID id = o.getID();
		return id;
	}

}
