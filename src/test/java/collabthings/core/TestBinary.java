package collabthings.core;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;

import org.collabthings.core.WClient;
import org.collabthings.core.model.objects.BinaryListener;
import org.collabthings.core.model.objects.WBinary;
import org.collabthings.core.utils.ConditionWaiter;
import org.collabthings.core.utils.WLogger;
import org.collabthings.core.utils.WPreferences;
import org.xml.sax.SAXException;

public final class TestBinary extends WCTestCase {
	private WLogger log = WLogger.getLogger(this);

	public void testBinary() throws IOException, SAXException {

		WBinary binary = new WBinary(getClient(false).getService(), getTempPath(), "test", "test");
		addData(binary);

		// More bytes in chunks
		for (int count = 0; count < 100; count++) {
			for (int i = 0; i < 100; i++) {
				byte byt = (byte) (i & 0xff);
				binary.add(Byte.valueOf(byt));
			}
		}
		binary.setReady();
		assertEquals(110000, binary.length());
		// second binary read with inputstream.
		WBinary binary2 = new WBinary(getClient(false).getService(), getTempPath(), "test2", "test");
		binary2.importStream(binary.getInputStream());
		binary2.setReady();

		assertBinariesAreSame(binary, binary2);

		assertTrue(binary2.isUsed(1000));
		assertFalse(binary2.isUsed(1));
	}

	private void addData(WBinary binary) throws IOException {
		for (int i = 0; i < 100000; i++) {
			byte byt = (byte) (i & 0xff);
			binary.add(Byte.valueOf(byt));
		}
	}

	private void assertBinariesAreSame(WBinary binary, WBinary binary2) throws IOException {
		byte[] bs1 = new byte[1000];
		byte[] bs2 = new byte[1000];

		InputStream is1 = binary.getInputStream();
		InputStream is2 = binary2.getInputStream();

		while (true) {
			int bcount = is2.read(bs2);
			int acount = is1.read(bs1);

			if (acount != bcount) {
				assertEquals(acount, bcount);
			}

			if (acount < 0) {
				break;
			} else {
				for (int i = 0; i < acount; i++) {
					assertEquals(bs1[i], bs2[i]);
				}
			}
		}
	}

	public void testAddBytes() throws IOException {
		WBinary b = getNewBinary();
		byte bs[] = new byte[2000];
		for (int i = 0; i < bs.length; i++) {
			bs[i] = (byte) (i & 0xff);
		}
		b.add(bs);
		b.addAt(1000, new byte[] { 1, 2, 3, 4 });
		b.setReady();

		InputStream is = b.getInputStream();
		for (int i = 0; i < bs.length; i++) {
			byte ib = (byte) (is.read() & 0xff);
			if (i >= 1000 && i <= 1003) {
				assertEquals("index:" + i, 1 + i - 1000, ib);
			} else {
				assertEquals("index:" + i, bs[i], ib);
			}
		}
	}

	public void testListener() {
		WBinary a = getNewBinary();
		final StringBuilder sb = new StringBuilder();
		a.addListener(new BinaryListener() {

			@Override
			public void ready(WBinary binary) {
				sb.append("ready");
			}
		});
		a.setReady();
		ConditionWaiter.wait(new ConditionWaiter.Condition() {
			@Override
			public boolean test() {
				return sb.length() > 0;
			}
		}, 10000);
		assertTrue(sb.length() > 0);
	}

	private WBinary getNewBinary() {
		try {
			return new WBinary(getClient(false).getService(), getTempPath(), "test", "test");
		} catch (MalformedURLException | SAXException e) {
			log.error(e);
			return null;
		}
	}

	public void testLoadSharedFile() throws SAXException, IOException {
		WClient c1 = getClient(false);
		WBinary b1 = c1.getService().getBinarySource().newBinary("test", "test");
		assertNotNull(b1);
		addData(b1);
		b1.setReady();
		b1.publish();
		//
		WPreferences p1 = c1.getPreferences();

		//
		WClient c2 = getClient(false);
		c2.getPreferences().set(WPreferences.LOCAL_PATH, p1.get(WPreferences.LOCAL_PATH, "FAIL"));

		WBinary b2 = c2.getService().getBinarySource().get(b1.getID());
		assertNotNull(b2);
		//
		assertNotNull(c2.getService().getBinarySource().get(b1.getID()));
	}

	public void testLoad() throws IOException {
		WBinary b1 = getNewBinary();
		addData(b1);
		b1.setReady();

		WBinary b2 = getNewBinary();
		b2.load(b1.getInputStream());
		b2.setReady();
	}
}
