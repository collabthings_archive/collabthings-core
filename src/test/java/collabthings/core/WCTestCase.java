package collabthings.core;

import java.net.MalformedURLException;

import org.collabthings.core.WClient;
import org.collabthings.core.test.TestEnv;
import org.collabthings.core.utils.WLogger;
import org.xml.sax.SAXException;

import junit.framework.TestCase;

public class WCTestCase extends TestCase {
	protected WLogger log = WLogger.getLogger(this);
	private TestEnv env = new TestEnv();

	protected void setUp() throws Exception {
		log.info("************************ STARTING A TEST " + this + " " + this.getName() + " ********");
		env.init();
	}

	protected void tearDown() throws Exception {
		log.info("************************* CLOSING " + this + " " + this.getName() + " ********");
		env.end();
	}

	protected WClient getClient(final boolean bind) throws MalformedURLException, SAXException {
		return env.getClient(bind);
	}

	protected String getTempPath() {
		return env.getTempPath();
	}
	
	protected void assertValue(String name) {
		assertNotNull(env.getValue(name));
	}

	public void testTrue() {
		assertTrue(true);
	}

	protected int getWaitTime() {
		if (System.getProperty("extended.debug") != null) {
			return 360000;
		} else {
			return 180000;
		}
	}

}
