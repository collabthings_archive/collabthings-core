package collabthings.core;

import org.collabthings.core.utils.ConditionWaiter;

import junit.framework.TestCase;

public class TestConditionWaiter extends TestCase {

	public void testWait() {
		ConditionWaiter w = ConditionWaiter.wait(
				new ConditionWaiter.Condition() {

					@Override
					public boolean test() {
						return true;
					}
				}, -1);
		assertTrue(w.isDone());
	}
}
