package collabthings.core;

import org.collabthings.core.utils.ConditionWaiter;
import org.collabthings.core.utils.ConditionWaiter.Condition;
import org.collabthings.core.utils.ThreadChecker;
import org.collabthings.core.utils.ThreadChecker.IChecker;

public class TestUtils extends WCTestCase {

	public void testThreadChecker() {
		final long st = System.currentTimeMillis();
		final Condition c = new Condition() {
			@Override
			public boolean test() {
				return (System.currentTimeMillis() - st) > 2000;
			}
		};

		new ThreadChecker(new IChecker() {

			@Override
			public boolean check() {
				return !c.test();
			}
		}, 100);

		final ConditionWaiter w = ConditionWaiter.wait(c, 10000);
	}
}
