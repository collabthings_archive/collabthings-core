package collabthings.core;

import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;

import org.collabthings.core.ServiceObject;
import org.collabthings.core.ServiceObjectData;
import org.collabthings.core.WClient;
import org.collabthings.datamodel.WData;
import org.collabthings.datamodel.WObject;
import org.collabthings.datamodel.WObjectID;
import org.collabthings.datamodel.WXML;
import org.collabthings.datamodel.WaazdohInfo;
import org.xml.sax.SAXException;

public class TestHash extends WCTestCase {

	ServiceObject o;

	public void testBeanHash() throws SAXException, IOException {
		WData abean = new WData("test");
		abean.addValue("testvalue", false);
		String ahash = abean.getContentHash();

		WData bbean = new WData(new WXML(new StringReader(abean.toXML()
				.toString())));

		String bhash = bbean.getContentHash();

		assertEquals(ahash, bhash);
	}

	public void testServiceObjectHash() throws SAXException,
			MalformedURLException {
		ServiceObjectData so = new ServiceObjectData() {

			@Override
			public boolean parse(WObject bean) {
				// FAILS
				return false;
			}

			@Override
			public WObject getObject() {
				WObject b = o.getBean();
				return b;
			}
		};

		WClient c = getClient(false);
		o = new ServiceObject("test", c, so, WaazdohInfo.VERSION, "WAAZDOHTEST");

		WObjectID id = o.getID();
		assertNotNull(id);

		String ahash = o.getHash();
		assertTrue(ahash.length() > 30);

		o.modified();

		String bhash = o.getHash();
		assertNotSame(ahash, bhash);
	}
}
