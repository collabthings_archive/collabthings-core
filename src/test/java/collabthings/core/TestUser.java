package collabthings.core;

import java.net.MalformedURLException;

import org.collabthings.core.WClient;
import org.collabthings.core.model.User;
import org.collabthings.datamodel.WUserID;
import org.xml.sax.SAXException;

public class TestUser extends WCTestCase {
	public void testGetUser() throws MalformedURLException, SAXException {
		WClient c = getClient(false);
		assertTrue(c.getUserID().toString().startsWith("@"));
		assertTrue(c.getUserID().toString().contains("=."));

		User user = c.getUser(c.getUserID());
		assertNotNull(user);
	}

	public void testFailGetUser() throws MalformedURLException, SAXException {
		WClient c = getClient(false);
		assertNull(c.getUser(new WUserID("FAIL")));
	}
}
