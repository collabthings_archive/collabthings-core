package collabthings.core;

import static org.junit.Assert.assertNotEquals;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

import org.collabthings.core.WClient;
import org.collabthings.core.utils.ConditionWaiter;
import org.xml.sax.SAXException;

public final class TestLists extends WCTestCase {

	public void testWrite() throws IOException, SAXException {
		WClient e = getClient(false);
		assertNotNull(e);
		//

		String path = "/testpath/testname";
		e.getStorage().write(path, "testvalue");
		Map<String, String> list = e.getStorage().getList(path);
		assertEquals(path, list.keySet().iterator().next());
		assertEquals("testvalue", list.values().iterator().next());

		String readvalue = e.getStorage().read(path);
		assertEquals("testvalue", readvalue);
	}

	public void testBothDirections() throws IOException, SAXException {
		WClient c1 = getClient(false);
		assertNotNull(c1);
		WClient c2 = getClient(false);
		assertNotNull(c2);
		//

		c2.getService().getUsers().follow(c1.getUserID().toString());
		c1.getService().getUsers().follow(c2.getUserID().toString());

		String data = "" + new Date();
		String path = "/hello_ba_friend";
		c2.getStorage().write(path, data);

		String c2path = c2.getUserID().toString() + path;

		assertNotEquals(data, c1.getStorage().read(c2path));

		ConditionWaiter cw = ConditionWaiter.wait(() -> {
			return false;
		}, 1000);


		cw = ConditionWaiter.wait(() -> {
			return data.equals(c1.getStorage().read(c2path));
		}, 10000);

		assertEquals(data, c1.getStorage().read(c2path));
	}

	public void testShared() throws IOException, SAXException {
		WClient c1 = getClient(false);
		assertNotNull(c1);
		WClient c2 = getClient(false);
		assertNotNull(c2);
		//

		c2.getService().getUsers().follow(c1.getUserID().toString());

		for (int i = 0; i < 20; i++) {
			c1.getStorage().write("/random/i" + i, "" + Math.random());
		}

		ConditionWaiter cw = ConditionWaiter.wait(() -> {
			return false;
		}, 5000);

		Map<String, String> c1read = c1.getStorage().getList("/");
		Map<String, String> c2read = c2.getStorage().getList(c1.getUserID().toString() + "/");

		log.info("c1read " + c1read);
		log.info("c2read " + c2read);

		for (String k : c1read.keySet()) {
			assertEquals(c1read.get(k), c2read.get(k));
		}

		for (String k : c1read.keySet()) {
			assertEquals(c1.getStorage().read(k), c2.getStorage().read(c1.getUserID().toString() + k));
		}
	}
}
