package collabthings.core;

import org.collabthings.core.storage.local.FileBeanStorage;
import org.collabthings.core.utils.StaticTestPreferences;
import org.collabthings.datamodel.WObject;
import org.collabthings.datamodel.WStringID;

public class TestFileBeanStorage extends WCTestCase {

	public void testIterator() {
		FileBeanStorage s = new FileBeanStorage(new StaticTestPreferences(
				"2015test" + getClass(), "2015test"
						+ System.currentTimeMillis()));
		WStringID madeupid = new WStringID("" + System.currentTimeMillis());

		assertNull(s.getBean(madeupid));

		String beanname = "test" + madeupid;
		s.addObject(madeupid, new WObject(beanname));
		Iterable<WStringID> i = s.getLocalSetIDs("2"); // TODO what about next
														// millenium?
		assertTrue(i.iterator().hasNext());
		WStringID id = i.iterator().next();
		assertNotNull(id);
		assertEquals(madeupid, id);
		//
		WObject b = s.getBean(id);
		assertEquals(beanname, b.getType());
	}

}
