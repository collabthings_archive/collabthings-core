package collabthings.core;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import org.collabthings.core.BinarySource;
import org.collabthings.core.WClient;
import org.collabthings.core.model.objects.WBinary;
import org.collabthings.core.utils.ConditionWaiter;
import org.collabthings.core.utils.WLogger;
import org.xml.sax.SAXException;

public class TestP2PBinarySource extends WCTestCase {

	private BinarySource serverb;
	private BinarySource servera;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		servera = null;
		serverb = null;
		WLogger.resetStartTime();
	}

	public void testSourcesConnected() throws MalformedURLException, SAXException {
		servera = getSource();
		serverb = getSource();

		servera.waitUntilReady();
		serverb.waitUntilReady();

		assertTrue(servera.isReady());
		assertTrue(servera.isRunning());
		assertTrue(serverb.isReady());
		assertTrue(serverb.isRunning());
	}

	private BinarySource getSource() throws MalformedURLException, SAXException {
		return getSource(servera == null);
	}

	private BinarySource getSource(boolean bind) throws MalformedURLException, SAXException {
		WClient c = getClient(bind);
		return c.getService().getBinarySource();
	}

	public void testget() throws SAXException, FileNotFoundException, IOException {
		servera = getSource();
//		getSource(true);

		WBinary binarya = servera.newBinary("comment", "bin");
		addBinaryData(binarya);

		serverb = getSource();
		final WBinary binaryb = serverb.get(binarya.getID());
		assertNotNull(binaryb);
		assertFalse(binaryb.isReady());

		serverb.waitUntilReady();

		ConditionWaiter.wait(new ConditionWaiter.Condition() {

			@Override
			public boolean test() {
				return binaryb.isReady();
			}
		}, getWaitTime());

		assertTrue(binaryb.isReady());
		assertEquals(1000, binaryb.length());
	}

	private void addBinaryData(WBinary binarya) throws IOException {
		binarya.add(new byte[1000]);
		binarya.setReady();
		binarya.publish();
	}
}
