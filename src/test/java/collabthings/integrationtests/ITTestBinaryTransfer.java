package collabthings.integrationtests;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.collabthings.core.BinarySource;
import org.collabthings.core.WClient;
import org.collabthings.core.model.objects.WBinary;
import org.collabthings.core.utils.ConditionWaiter;
import org.collabthings.core.utils.StaticTestPreferences;
import org.collabthings.core.utils.WLogger;
import org.junit.Test;
import org.xml.sax.SAXException;

import collabthings.core.WCTestCase;

public final class ITTestBinaryTransfer extends WCTestCase {
	private WLogger log = WLogger.getLogger(this);

	public void testTransfer10kAB() throws SAXException, InterruptedException, IOException {
		testTransfer(10000, true, false);
	}

	public void testTransfer10kBA() throws SAXException, InterruptedException, IOException {
		testTransfer(10000, false, true);
	}

	public void testTransfer10k() throws SAXException, InterruptedException, IOException {
		testTransfer(10000);
	}

	public void testTransfer30k() throws SAXException, InterruptedException, IOException {
		testTransfer(30000);
	}

	public void testTransfer75k() throws SAXException, InterruptedException, IOException {
		testTransfer(75000);
	}

	public void testTransfer100k() throws SAXException, InterruptedException, IOException {
		testTransfer(100000);
	}

	public void testTransfer300k() throws SAXException, InterruptedException, IOException {
		testTransfer(300000);
	}

	public void testTransfer1M() throws SAXException, InterruptedException, IOException {
		testTransfer(1000000);
	}

	public void testTransfer5M() throws SAXException, InterruptedException, IOException {
		testTransfer(5000000);
	}

	private void testTransfer(int i) throws SAXException, InterruptedException, IOException {
		testTransfer(i, true, false);
	}

	@Test
	public void testTransfer(int binarysize, boolean bind1, boolean bind2)
			throws SAXException, InterruptedException, IOException {
		log.info("test transfer " + binarysize);

		WClient c1 = getClient(bind1);
		WClient c2 = getClient(bind2);

		c2.getService().getBinarySource().waitUntilReady();

		testTransfer(binarysize, c1.getService().getBinarySource(), c2.getService().getBinarySource());

	}

	private void testTransfer(int binarysize, BinarySource source1, BinarySource source2)
			throws FileNotFoundException, IOException {
		log.info("creating binary");
		WBinary b1 = source1.newBinary("test", "bin");
		assertNotNull(b1);
		log.info("adding bytes");
		byte bs[] = new byte[binarysize];
		for (int i = 0; i < binarysize; i++) {
			bs[i] = (byte) (i & 0xff);
		}
		b1.add(bs);

		log.info("publishing " + b1);
		b1.setReady();
		b1.publish();
		assertEquals(binarysize, b1.length());
		String b1hasht = b1.getHash();

		log.info("get source1");
		WBinary b1reload = source1.get(b1.getID());

		assertNotNull(b1reload);
		assertFalse(b1reload.isReady());

		ConditionWaiter.wait(new ConditionWaiter.Condition() {
			@Override
			public boolean test() {
				return b1reload.isReady();
			}
		}, getWaitTime());

		assertTrue(b1reload.isReady());

		assertEquals(b1.getObject().toYaml(), b1reload.getObject().toYaml());
		assertEquals(b1.getObject().toText(), b1reload.getObject().toText());
		assertEquals(b1.getHash(), b1reload.getHash());
		assertEquals(b1hasht, b1reload.getHash());

		// 2

		WBinary b2 = source2.get(b1.getID());
		assertNotNull(b2);
		log.info("wait until ready");

		long st = System.currentTimeMillis();
		while (!b2.isReady() && System.currentTimeMillis() - st < 120000) {
			doWait(100);
		}

		log.info("checks " + b2);
		assertTrue(b2.isReady());
		assertEquals(b1, b2);
		log.info("closing");

		source1.close();
		source2.close();
	}

	private synchronized void doWait(int i) {
		try {
			this.wait(i);
		} catch (InterruptedException e) {
			log.error(e);
		}
	}

	@Override
	protected void tearDown() throws Exception {
		StaticTestPreferences.clearPorts();
		super.tearDown();
	}
}
