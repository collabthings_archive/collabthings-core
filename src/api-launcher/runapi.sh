#!/bin/bash

number=$1
if [ -z "$1" ]; then
	number=006
fi

cd $(dirname $0)

echo CORE LAUNCHING API ${number}

docker network create -d bridge test-net
docker run  --name ct-api${number} -p 14${number}:14881 --network=test-net jeukku/ct-api:latest
