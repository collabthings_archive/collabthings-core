/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen
 ******************************************************************************/
package org.collabthings.core.app;

import java.util.List;

import org.collabthings.core.Login;
import org.collabthings.core.WClient;
import org.collabthings.core.model.objects.WBinary;
import org.collabthings.core.utils.WLogger;
import org.collabthings.datamodel.ObjectVO;
import org.collabthings.datamodel.StorageAreaVO;
import org.collabthings.datamodel.UserVO;
import org.collabthings.datamodel.WObject;
import org.collabthings.datamodel.WStringID;

public class AppLauncher {
	private WClient client;
	private WLogger log = WLogger.getLogger(this);
	private String prefix;

	public AppLauncher(String string) {
		this.prefix = string;
	}

	private void start() {
		Login login = new Login();
		String username = "downloader";
		client = login.login("downloadeverything-" + prefix, username);
		// client.getBinarySource().addMessageHandler(WMessenger.MESSAGENAME_PUBLISHED,
		// new PublishedHandler());
		// client.getPreferences().set(P2PServer.DOWNLOAD_EVERYTHING, true);

		new Thread(() -> {
			try {
				run();
			} catch (InterruptedException e) {
				log.error(e);
				Thread.currentThread().interrupt();
			}
		}).start();
	}

	private synchronized void run() throws InterruptedException {
		log.info("running with " + client);

		while (client.isRunning()) {
			log.info("still running with " + client);
			runLoop();
			wait(60000);
		}

		log.info("Done " + client);
	}

	private synchronized void runLoop() {
		try {
			List<UserVO> users = client.getService().getUsers().search("user", 10);
			for (UserVO user : users) {
				readUsersNewItems(user);
			}
		} catch (Exception e) {
			log.error(e);
		}
	}

	private void readUsersNewItems(UserVO user) {
		List<String> newitems = client.getService().getStorageArea().listNewItems(user.getUserid(), 0, 10);
		for (String path : newitems) {
			StorageAreaVO vo = new StorageAreaVO();
			vo.setUsername(user.getUsername());
			vo.setPath(path);

			String oid = client.getService().getStorageArea().read(path);
			if (oid != null) {
				read(oid);
			}
		}
	}

	private void read(String oid) {
		ObjectVO o = client.getService().getObjects().read(oid);
		if (o.isSuccess()) {
			WObject wo = o.toObject();
			if (wo.getType().equals(WBinary.BEAN_TAG)) {
				log.info("binary " + oid);
				client.getService().getBinarySource().get(new WStringID(oid));
			}
		}
	}

	public static void main(String[] args) {
		AppLauncher d = new AppLauncher(args[0]);
		d.start();
	}

	/*
	 * private class PublishedHandler implements MMessageHandler { private
	 * WMessenger setMessenger;
	 * 
	 * @Override public MMessage handle(MMessage m) { WStringID binaryid =
	 * m.getIDAttribute("binaryid"); client.getBinarySource().get(new
	 * WStringID(binaryid)); return setMessenger.newResponseMessage(m, "ok"); }
	 * 
	 * @Override public void setMessenger(WMessenger factory) { this.setMessenger =
	 * factory; } }
	 */
}
