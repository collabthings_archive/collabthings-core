/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen - initial API and implementation
 ******************************************************************************/
package org.collabthings.core;

import java.util.ArrayList;
import java.util.List;

import org.collabthings.common.service.ObjectsService;
import org.collabthings.core.utils.WLogger;
import org.collabthings.datamodel.ObjectVO;
import org.collabthings.datamodel.ReturnVO;
import org.collabthings.datamodel.WHashSource;
import org.collabthings.datamodel.WObject;
import org.collabthings.datamodel.WObjectID;
import org.collabthings.datamodel.WStringID;
import org.collabthings.datamodel.WUserID;

public final class ServiceObject implements WHashSource {
	private WUserID creatorid;

	private WObjectID id;
	private long creationtime = System.currentTimeMillis();

	private ServiceObjectData data;

	private WLogger log = WLogger.getLogger(this);

	private String tagname;
	private WStringID copyof;
	private WObject storedbean = new WObject();

	private ObjectsService objects;

	private List<ServiceObjectListener> listeners = new ArrayList<>();

	private String version;

	private String prefix;

	private String lastpublishedid;

	private Long modified;

	private String lastsavedid;

	private WUserID userid;

	public ServiceObject(final String tagname, final WUserID nuserid, final ObjectsService objects,
			final ServiceObjectData data, final String version, final String nprefix) {
		this.tagname = tagname;
		this.creatorid = nuserid;
		this.userid = nuserid;

		this.data = data;
		this.objects = objects;
		this.creationtime = System.currentTimeMillis();
		this.modified = System.currentTimeMillis();
		this.version = version;
		this.prefix = nprefix;

		id = new WObjectID(this, prefix);
	}

	public ServiceObject(final String tagname, final WClient client, final ServiceObjectData data, final String version,
			final String nprefix) {
		this.tagname = tagname;
		this.creatorid = client.getUserID();
		this.userid = client.getUserID();

		this.data = data;
		this.objects = client.getObjects();
		this.creationtime = System.currentTimeMillis();
		this.modified = System.currentTimeMillis();
		this.version = version;
		this.prefix = nprefix;

		id = new WObjectID(this, prefix);
	}

	public boolean load(WStringID oid) {
		if (oid == null) {
			return false;
		}

		ObjectVO ovo = objects.read(oid.toString());
		WObject wo = new WObject();
		if (ovo != null) {
			wo.parse(ovo.getData());
			if(!objects.filter(wo)) {
				return false;
			}
			
			storedbean = wo;
			lastpublishedid = oid.toString();
		} else {
			return false;
		}

		id = new WObjectID(oid, this);

		boolean parsingok = parseObject(wo);
		id.setStoredhash(ovo.getHash());

		if (parsingok) {
			if (id.isID(oid)) {
				return true;
			} else {
				StringBuilder sb = new StringBuilder();
				sb.append(
						"ERROR: Loaded object id not equal to requested. Some part of object propable failed to parse. Requested:"
								+ oid + " Result:" + id);
				sb.append("\nObject got " + wo.toYaml());
				sb.append("\nObject created " + data.getObject().toYaml());
				log.info(sb.toString());

				return true;
			}
		} else {
			return false;
		}
	}

	private boolean parseObject(WObject o) {
		id = new WObjectID(o.getAttribute("id"), this);

		creatorid = o.getUserAttribute("creator");
		creationtime = o.getLongValue("creationtime");
		version = o.getValue("version");
		copyof = o.getIDValue("copyof");
		//
		boolean success = data.parse(o);

		modified = o.getLongValue("modified");

		return success;
	}

	public WObjectID getID() {
		return id;
	}

	public WObject getBean() {
		WObject bt = new WObject(tagname);
		bt.setAttribute("id", id.getBase());

		bt.addValue("creationtime", getCreationtime());
		if (modified != null && modified > 0) {
			bt.addValue("modified", modified);
		}
		bt.addValue("creator", creatorid.toString());
		bt.addValue("version", version);
		bt.addValue("license", "GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html");
		if (copyof != null) {
			bt.addValue("copyof", copyof.toString());
		}

		return bt;
	}

	@Override
	public String toString() {
		return "ServiceObject[" + tagname + "][" + id + "]";
	}

	public long getCreationtime() {
		return creationtime;
	}

	public boolean publish() {
		save();

		String sid = id.toString();
		if (lastpublishedid == null || !lastpublishedid.equals(sid)) {
			long st = System.currentTimeMillis();
			log.info("publishing " + st + " id:" + id);
			lastpublishedid = sid;
			ReturnVO ret = objects.publish(sid);
			log.info("published " + ret + " dtime:" + (System.currentTimeMillis() - st));
			return ret.isSuccess();
		} else {
			return true;
		}
	}

	@Override
	public String getHash() {
		return data.getObject().getContentHash();
	}

	public void save() {
		if (!userid.equals(creatorid)) {
			copyof = getID().getStringID();
			id = new WObjectID(this, prefix);
			creatorid = userid;
		}

		WObject current = data.getObject();
		String sid = id.toString();
		if (storedbean == null || !storedbean.equals(current)) {
			if (storedbean != null) {
				log.info("" + sid + " stored " + storedbean.toYaml());
				log.info("" + sid + " stored " + storedbean.getContentHash());
			}

			log.info("" + sid + " current " + current.toYaml());
			log.info("" + sid + " current " + current.getContentHash());

			modified();

			sid = id.getContentId();
			WObject storing = data.getObject();
			log.info("" + id + " storing " + storing.toYaml());
			//
			storedbean = storing;

			log.info("writing to service " + sid);
			String yaml = storing.toYaml();
			if (yaml.indexOf("_content") >= 0) {
				log.info("yaml:" + yaml);
				throw new RuntimeException("yaml contains _content");
			}
			
			ObjectVO vo = objects.write(sid, yaml);
			log.info("stored " + sid + " stored hash " + vo.getHash());
			this.id.setStoredhash(vo.getHash());

			this.lastsavedid = sid;
		}
	}

	public void modified() {
		modified = System.currentTimeMillis();
		List<ServiceObjectListener> ls = new ArrayList<>(listeners);
		for (ServiceObjectListener trackListener : ls) {
			trackListener.modified();
		}
	}

	public void addListener(ServiceObjectListener trackListener) {
		listeners.add(trackListener);
	}

	public boolean hasChanged() {
		if (lastsavedid == null || !lastsavedid.equals(id.getContentId())) {
			log.info("service object has changed " + getBean().toYaml());
			log.info("service object has changed " + this.objects.read(id.toString()));

			return true;
		} else {
			return false;
		}
	}

	public WStringID getCopyOf() {
		return copyof;
	}

	public long getModified() {
		return modified;
	}
}
