/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen
 ******************************************************************************/
package org.collabthings.core;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.collabthings.common.service.ObjectsService;
import org.collabthings.common.service.StorageAreaService;
import org.collabthings.core.model.User;
import org.collabthings.core.utils.WLogger;
import org.collabthings.core.utils.WPreferences;
import org.collabthings.datamodel.ObjectVO;
import org.collabthings.datamodel.StorageAreaVO;
import org.collabthings.datamodel.UserVO;
import org.collabthings.datamodel.WObject;
import org.collabthings.datamodel.WUserID;

public class WClient {
	private WServiceClient service;
	private WPreferences preferences;
	private boolean running = true;
	//
	private Set<WClientListener> listeners = new HashSet<WClientListener>();

	private WLogger logger = WLogger.getLogger(this);

	public static interface Filter {
		boolean check(WObject o);
	}

	public WClient(WPreferences p, WServiceClient nservice) {
		this.preferences = p;
		this.service = nservice;
		StringBuilder sb = new StringBuilder();

		sb.append("****** WClient *******\n");
		sb.append("\t" + service.toString());
		sb.append("\t" + p.toString());

		logger.info(sb.toString());
	}

	public boolean isRunning() {
		if (service == null) {
			return false;
		}

		return running;
	}

	public WUserID getUserID() {
		return new WUserID(this.service.getUser().getUserid());
	}

	public WServiceClient getService() {
		return service;
	}

	public String getInfoText() {
		return service.getBinarySource().getInfoText();
	}

	public WPreferences getPreferences() {
		return preferences;
	}

	public void stop() {
		running = false;
		this.service.close();
	}

	public void addListener(WClientListener listener) {
		listeners.add(listener);
	}

	private void loggedIn() {
		for (WClientListener clientListener : listeners) {
			clientListener.loggedIn();
		}
	}

	public String readStorageArea(String string) {
		return getService().getStorageArea().read(getService().getUser().getUserid(), string);
	}

	public List<ObjectVO> search(String searchitem, int index, int count) {
		return getService().getObjects().search(searchitem, index, count);
	}

	public List<UserVO> searchUsers(String searchitem, int index) {
		return getService().getUsers().search(searchitem, index);
	}

	public User getUser(WUserID userID) {
		UserVO r = getService().getUsers().getUser(userID.toString());
		if (r != null && r.isSuccess()) {
			return new User(r);
		} else {
			return null;
		}
	}

	@Override
	public String toString() {
		UserVO user = getService().getUser();
		String username = user != null ? user.getUsername() : "unknown";
		return "WClient[" + username + "][connected:" + isRunning() + "]";
	}

	public ObjectsService getObjects() {
		return getService().getObjects();
	}

	public void addObjectFilter(Filter f) {
		getObjects().addFilter(f);
	}

	public BinarySource getBinarySource() {
		return getService().getBinarySource();
	}

	public StorageAreaService getStorage() {
		return getService().getStorageArea();
	}
}
