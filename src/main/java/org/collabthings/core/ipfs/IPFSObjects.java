package org.collabthings.core.ipfs;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.collabthings.common.service.ObjectsService;
import org.collabthings.core.BeanStorage;
import org.collabthings.core.WClient;
import org.collabthings.core.WClient.Filter;
import org.collabthings.datamodel.ObjectVO;
import org.collabthings.datamodel.ReturnVO;
import org.collabthings.datamodel.WObject;
import org.collabthings.datamodel.WStringID;
import org.collabthings.datamodel.WaazdohInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.ipfs.api.IPFS;
import io.ipfs.api.MerkleNode;
import io.ipfs.api.NamedStreamable;
import io.ipfs.multihash.Multihash;

public class IPFSObjects implements ObjectsService {
	private Logger log = LoggerFactory.getLogger(this.getClass());

	private IPFS ipfs;

	private List<BeanStorage> storages = new LinkedList<>();
	private List<Filter> filters = new ArrayList<WClient.Filter>();

	public IPFSObjects(IPFS ipfs) {
		this.ipfs = ipfs;
	}

	@Override
	public void addBeanStorage(BeanStorage bstorage) {
		storages.add(bstorage);
	}

	@Override
	public ObjectVO read(String string) {
		String[] split = string.split("_");

		String sid = split[0];
		int index = 1;
		while (index < split.length && index < 3) {
			sid += "_" + split[index++];
		}

		if (string.indexOf(
				"201608CT_c7834e1e-d6a7-4ffc-af38-8cac208df08a_A64fb196238d9cba18152430caf65681d44e1e87236f1ec614a81b075e762b2d3") >= 0) {
			log.info("FOUND");
		}

		WStringID oldid = new WStringID(sid);
		for (BeanStorage bs : storages) {
			WObject b = bs.getBean(oldid);
			if (b != null) {
				ObjectVO ovo = new ObjectVO(b.toYaml());
				return ovo;
			}
		}

		if (split.length > 3) {
			String ipfshash = split[3];
			try {
				if (ipfshash.length() > 20) {
					log.info("getting object with hash " + ipfshash + " id:" + string);
					byte[] content = ipfs.cat(Multihash.fromBase58(ipfshash));
					ObjectVO ovo = new ObjectVO(new String(content, Charset.forName(WaazdohInfo.CHARSET)));
					ovo.setHash(ipfshash);
					return ovo;
				} else {
					ObjectVO error = new ObjectVO();
					error.setSuccess(false);
					error.setMessage("hash not qualified");
					return error;
				}
			} catch (IOException e) {
				log.error("read " + string, e);
				return null;
			}
		} else {
			log.info("failed to find object with id " + string);
			return null;
		}
	}

	@Override
	public List<ObjectVO> search(String search, int i, int j) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectVO write(String objectid, String data) {
		NamedStreamable.ByteArrayWrapper file = new NamedStreamable.ByteArrayWrapper(objectid + ".yml",
				data.getBytes());

		try {
			List<MerkleNode> ns = ipfs.add(file);
			Multihash hash = ns.get(0).hash;
			log.info("Added objectid:" + objectid + " ipfs hash:" + hash.toString());

			log.info("got " + new String(ipfs.cat(hash), Charset.forName("UTF-8")));

			ObjectVO ret = new ObjectVO();
			ret.setSuccess(true);
			ret.setHash(hash.toString());

			return ret;
		} catch (IOException e) {
			log.error("write " + objectid, e);
			return null;
		}

	}

	@Override
	public ReturnVO publish(String objectid) {
		return ReturnVO.getTrue();
	}

	@Override
	public ReturnVO error(String id, String message) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean filter(WObject o) {
		for (Filter filter : filters) {
			if (!filter.check(o)) {
				return false;
			}
		}

		return true;
	}

	@Override
	public void addFilter(Filter f) {
		filters.add(f);
	}

}
