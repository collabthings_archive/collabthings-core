package org.collabthings.core.ipfs;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.collabthings.common.service.StorageAreaService;
import org.collabthings.core.utils.WLogger;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import io.ipfs.api.IPFS;

public class IPFSStorageArea implements StorageAreaService {
	private WLogger log = WLogger.getLogger(this);

	private IPFS ipfs;
	private RestTemplate rest = new RestTemplate();
	private final String apiurl;
	private JsonParser p = new JsonParser();

	public IPFSStorageArea(String apiurl, IPFS ipfs) {
		this.ipfs = ipfs;
		this.apiurl = apiurl;
	}

	@Override
	public List<String> listNewItems(String userid, int start, int count) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, String> getList(String path) {
		try {
			String sresult = rest.getForObject(apiurl + "/lists/get/{path}", String.class,
					URLEncoder.encode(path, "UTF-8"));
			log.info("getList json " + sresult);

			JsonElement result = p.parse(sresult);
			Map<String, String> ret = new HashMap<>();
			JsonObject asJsonObject = result.getAsJsonObject();
			for (String e : asJsonObject.keySet()) {
				ret.put(e, asJsonObject.get(e).getAsString());
			}

			return ret;
		} catch (RestClientException | UnsupportedEncodingException e) {
			log.error(e);
			return null;
		}
	}

	@Override
	public String read(String path) {
		return this.getList(path).get(removeUserId(path));
	}

	private String removeUserId(String path) {
        if(path.indexOf("@")==0) {
            int edindex= path.indexOf("=.ed");
            int slashindex = path.indexOf("/", edindex + 3);
            
            String userid = path.substring(0, slashindex);
            String npath = path.substring(slashindex);
            log.info("starts with a userid " + userid + " path:" + npath);
            return npath;
        } else {
        	return path;
        }
	}

	@Override
	public String read(String userid, String string) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void write(String path, String data) {
		try {
			MultipartBodyBuilder builder = new MultipartBodyBuilder();
			builder.part("path", path);
			builder.part("data", data);

			Map<String, String> m = new HashMap<>();
			m.put("path", path);
			m.put("data", data);

			String ok = rest.postForObject(apiurl + "/lists/write", m, String.class);
		} catch (RestClientException e) {
			log.error(e);
		}

	}

}
