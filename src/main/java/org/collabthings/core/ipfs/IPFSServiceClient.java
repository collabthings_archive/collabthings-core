package org.collabthings.core.ipfs;

import java.io.IOException;

import org.collabthings.common.service.ObjectsService;
import org.collabthings.common.service.StorageAreaService;
import org.collabthings.common.service.UsersService;
import org.collabthings.core.BinarySource;
import org.collabthings.core.WServiceClient;
import org.collabthings.core.utils.WPreferences;
import org.collabthings.datamodel.UserVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.ipfs.api.IPFS;

public class IPFSServiceClient implements WServiceClient {
	public static final String IPFS_CONNECTION = "ipfs.connectionV2";
	public static final String IPFS_LOCALPATH = "ipfs.localpath";
	public static final String API_URL = "api.url";

	private Logger log = LoggerFactory.getLogger(this.getClass());

	private WPreferences p;
	private IPFS ipfs;
	private StorageAreaService storagearea;
	private UsersService users;
	private ObjectsService objects;
	private IPFSBinarySource binarysource;
	private String apiurl;
	private UserVO userinfo;

	public IPFSServiceClient(WPreferences p) {
		this.p = p;

		this.apiurl = p.get(API_URL, "http://localhost:14881");

		String defaultvalue = "/ip4/127.0.0.1/tcp/5001";
		String connection = p.get(IPFS_CONNECTION, defaultvalue);
		if (!connection.startsWith("/")) {
			p.set(IPFS_CONNECTION, "");
			connection = defaultvalue;
		}

		log.info("starting ipfs with connection " + connection);
		ipfs = new IPFS(connection);
		binarysource = new IPFSBinarySource(this, ipfs, p);

		// getting user info
		initUserInfo(p);
	}

	private void initUserInfo(WPreferences pref) {
		log.info("initialize user info");
		this.userinfo = getUsers().getProfile().getUser();
	}

	public String getAddress() throws IOException {
		String connection = p.get(IPFS_CONNECTION, "/ip4/127.0.0.1/tcp/5001");
		connection += "/ipfs/FAIL";
		return connection;
	}

	@Override
	public StorageAreaService getStorageArea() {
		if (storagearea == null) {
			storagearea = new IPFSStorageArea(apiurl, ipfs);
		}
		return storagearea;
	}

	@Override
	public UsersService getUsers() {
		if (users == null) {
			users = new IPFSUsers(apiurl);
		}
		return this.users;
	}

	@Override
	public ObjectsService getObjects() {
		if (objects == null) {
			objects = new IPFSObjects(ipfs);
		}
		return objects;
	}

	@Override
	public BinarySource getBinarySource() {
		return binarysource;
	}

	@Override
	public void close() {
		// nothing todo
	}

	@Override
	public UserVO getUser() {
		UserVO vo = new UserVO();

		vo.setUserid(userinfo.getUserid());
		vo.setUsername(p.get("username", "user_unknown"));

		return vo;
	}

	@Override
	public UserVO getUser(String userid) {
		return users.getUser(userid);
	}

	public void connect(String string) {
		log.info("connect addr " + string);
	}

}
