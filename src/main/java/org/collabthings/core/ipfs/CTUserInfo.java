package org.collabthings.core.ipfs;

import com.google.gson.JsonElement;

public class CTUserInfo {

	private String userid;

	public CTUserInfo(JsonElement userinfo) {
		this.userid = userinfo.getAsJsonObject().get("userid").getAsString();
	}

	public String getID() {
		return this.userid;
	}
}
