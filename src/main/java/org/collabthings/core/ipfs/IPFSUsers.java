package org.collabthings.core.ipfs;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import org.collabthings.common.service.UsersService;
import org.collabthings.datamodel.ProfileVO;
import org.collabthings.datamodel.ReturnVO;
import org.collabthings.datamodel.UserVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class IPFSUsers implements UsersService {
	private Logger log = LoggerFactory.getLogger(this.getClass());

	private String apiurl;

	public IPFSUsers(String apiurl) {
		this.apiurl = apiurl;
	}

	@Override
	public ProfileVO getProfile() {
		try {
			String cmd = "me";
			JsonElement userinfo = callApi(cmd);

			ProfileVO prof = new ProfileVO();
			UserVO uservo = new UserVO();
			uservo.setUserid(userinfo.getAsJsonObject().get("userid").getAsString());
			prof.setUser(uservo);

			return prof;
		} catch (IOException e) {
			log.error("initUserInfo " + apiurl, e);
		}

		return null;
	}

	private JsonElement callApi(String cmd) throws MalformedURLException, IOException {
		URL u = new URL(apiurl + "/" + cmd);
		InputStream is = (InputStream) u.getContent();
		JsonParser p = new JsonParser();
		JsonElement json = p.parse(new InputStreamReader(is));
		log.info("json " + json);
		return json;
	}

	@Override
	public UserVO getUser(String userid) {
		JsonElement json;
		try {
			json = callApi("user/" + URLEncoder.encode(userid, "UTF-8"));
			UserVO user = new UserVO();
			user.setUserid(json.getAsJsonObject().get("userid").getAsString());
			return user;
		} catch (IOException e) {
			log.error("getUser", e);
			return null;
		}
	}

	@Override
	public UserVO getWithName(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<UserVO> search(String string, int i) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<UserVO> newUsers(int count) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ReturnVO saveProfile(ProfileVO profile) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void follow(String userid) {
		JsonElement json;
		try {
			json = callApi("users/follow/" + URLEncoder.encode(userid, "UTF-8"));
			log.info("follow return value " + json);
		} catch (IOException e) {
			log.error("getUser", e);
		}
	}

	@Override
	public List<String> getFollowing() {
		// TODO Auto-generated method stub
		return null;
	}

}
