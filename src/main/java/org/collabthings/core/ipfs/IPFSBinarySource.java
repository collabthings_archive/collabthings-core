package org.collabthings.core.ipfs;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.collabthings.core.BinarySource;
import org.collabthings.core.ReportingService;
import org.collabthings.core.WServiceClient;
import org.collabthings.core.model.objects.WBinary;
import org.collabthings.core.utils.ConditionWaiter;
import org.collabthings.core.utils.WPreferences;
import org.collabthings.datamodel.WStringID;
import org.collabthings.datamodel.WaazdohInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.ipfs.api.IPFS;
import io.ipfs.api.MerkleNode;
import io.ipfs.api.NamedStreamable;
import io.ipfs.api.NamedStreamable.ByteArrayWrapper;
import io.ipfs.multihash.Multihash;

public class IPFSBinarySource implements BinarySource {

	private WPreferences p;
	private IPFS ipfs;
	private ReportingService reportingservice;
	private WServiceClient client;

	private Map<WStringID, WBinary> binaries = new HashMap<>();
	private Logger log = LoggerFactory.getLogger(this.getClass());

	public IPFSBinarySource(WServiceClient client, IPFS ipfs2, WPreferences p) {
		this.p = p;
		this.ipfs = ipfs2;
		this.client = client;
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isRunning() {
		return true;
	}

	@Override
	public WBinary get(WStringID streamid) {
		if (streamid != null) {
			WBinary b = binaries.get(streamid);
			if (b == null) {
				b = newBinary("", "");
				if (b.load(streamid)) {
					binaries.put(streamid, b);
					return b;
				}
			}
			return b;
		} else {
			return null;
		}
	}

	@Override
	public void clearMemory(int suggestedmemorytreshold) {
	}

	@Override
	public String getInfoText() {
		// TODO
		return "IPFSBinarySource TODO";
	}

	@Override
	public void setReportingService(ReportingService rservice) {
		this.reportingservice = rservice;
	}

	@Override
	public WBinary newBinary(String comment, String extension) {
		return new WBinary(client, "./storage", comment, extension);
	}

	@Override
	public void waitUntilReady() {
		ConditionWaiter.wait(() -> isReady(), WaazdohInfo.DEFAULT_TIMEOUT);
	}

	@Override
	public boolean isReady() {
		try {
			return ipfs.version() != null;
		} catch (IOException e) {
			log.error("isReady version", e);
			return false;
		}
	}

	@Override
	public String writeData(InputStream inputStream) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			byte bs[] = new byte[1000];
			while (true) {
				int count = inputStream.read(bs);
				if (count <= 0) {
					break;
				}

				baos.write(bs, 0, count);
			}

			ByteArrayWrapper wr = new NamedStreamable.ByteArrayWrapper(baos.toByteArray());

			List<MerkleNode> s = ipfs.add(wr);
			String hash = s.get(0).hash.toString();
			log.info("wrote " + baos.size() + " bytes with hash " + hash);
			return hash;
		} catch (IOException e) {
			LoggerFactory.getLogger(this.getClass()).error("writeDate", e);
			return null;
		}
	}

	@Override
	public byte[] readData(String ipfsid) {
		try {
			this.log.info("readData " + ipfsid);
			byte[] bs = ipfs.cat(Multihash.fromBase58(ipfsid));
			log.info("got " + bs.length + "' bytes with " + ipfsid);
			return bs;
		} catch (IOException e) {
			LoggerFactory.getLogger(this.getClass()).error("readData " + ipfsid, e);
			return null;
		}
	}

	@Override
	public void published(WStringID id) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getStats() {
		// TODO Auto-generated method stub
		return null;
	}

}
