/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen
 ******************************************************************************/
package org.collabthings.core;

import java.io.InputStream;

import org.collabthings.core.model.objects.WBinary;
import org.collabthings.datamodel.WStringID;

public interface BinarySource {
	void close();

	boolean isRunning();

	WBinary get(WStringID streamid);

	void clearMemory(int suggestedmemorytreshold);

	String getInfoText();

	void setReportingService(ReportingService rservice);

	WBinary newBinary(final String comment, String extension);

	void waitUntilReady();

	boolean isReady();

	void published(WStringID id);

	String getStats();

	String writeData(InputStream inputStream);

	byte[] readData(String ipfsid);

}
