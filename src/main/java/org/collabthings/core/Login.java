/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen
 ******************************************************************************/
package org.collabthings.core;

import org.collabthings.core.ipfs.IPFSServiceClient;
import org.collabthings.core.utils.PropertiesPreferences;
import org.collabthings.core.utils.WLogger;

public class Login {

	private PropertiesPreferences p;
	private WLogger log;

	public WClient login(String prefix, String username) {
		log = WLogger.getLogger(this);

		int tries = 0;
		while (tries++ < 10) {
			return tryLogin(prefix, username);
		}

		return null;
	}

	private WClient tryLogin(String prefix, String username) {
		p = new PropertiesPreferences(prefix + "/" + username);
		WServiceClient serviceclient = new IPFSServiceClient(p);
		WClient client = new WClient(p, serviceclient);
		return client;
	}

	public PropertiesPreferences getPreferences() {
		return p;
	}
}
