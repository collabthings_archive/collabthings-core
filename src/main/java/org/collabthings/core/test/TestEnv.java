package org.collabthings.core.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.collabthings.core.WClient;
import org.collabthings.core.ipfs.IPFSServiceClient;
import org.collabthings.core.utils.ConditionWaiter;
import org.collabthings.core.utils.StaticTestPreferences;
import org.collabthings.core.utils.WLogger;
import org.collabthings.core.utils.WPreferences;
import org.collabthings.core.utils.WTimedFlag;
import org.xml.sax.SAXException;

public class TestEnv {
	private static final String PREFERENCES_PREFIX = "wcclienttests";
	protected WLogger log = WLogger.getLogger(this);
	private Map<String, String> values = new HashMap<String, String>();

	private static List<IPFSServiceClient> ipfsclients = new LinkedList<>();
	private static List<SSBProcesses> ssbprocesses = new LinkedList<>();

	private int usernamecounter = 0;
	private boolean stopped;

	public void init() {
		values.clear();
		stopped = false;

		StaticTestPreferences.clearPorts();
	}

	private synchronized String runSsb() throws IOException {
		int number = 700 + usernamecounter;
		String apiurl = getApiURL(number);
		URL url = new URL(apiurl + "/me");

		if (ssbprocesses.size() <= usernamecounter) {
			SSBProcesses ps = new SSBProcesses();

			log.info("current path " + new File(".").getAbsolutePath());
			log.info("running docker with port 14" + number);

			String dockercmd = "docker run --name ct-api" + number + " -p 14" + number
					+ ":14881 --network=test-net jeukku/ct-api:latest";
			log.info("cmd " + dockercmd);
			Process apip = Runtime.getRuntime().exec(dockercmd);
			readStream("ssb api " + number + " IS ", apip, apip.getInputStream());
			readStream("ssb api " + number + " ERROR ", apip, apip.getErrorStream());
			ps.api = apip;

			ssbprocesses.add(ps);
		} else {
			log.info("not running again ssb with port " + number);
		}

		try {
			boolean ok = false;
			int waitcount = 0;
			while (waitcount++ < 20) {
				wait(1000);

				try {
					URLConnection c = url.openConnection();
					log.info("url getContent " + c);

					BufferedReader in = new BufferedReader(new InputStreamReader(c.getInputStream()));
					String line = in.readLine();
					log.info("line " + line);
					if (line.contains("userid")) {
						ok = true;
						in.close();
						break;
					}
				} catch (IOException e) {
					log.info("getContent " + url + " exception:" + e);
				}
			}
			if (ok) {
				log.info("CT Api running in port 14" + number);
				return apiurl;
			} else {
				log.info("CT Api not found 14" + number);
				return null;
			}
		} catch (InterruptedException e) {
			log.error(e);
			return null;
		}

	}

	protected String getApiURL(int number) {
		return "http://localhost:14" + number;
	};

	WPreferences getPreferences(String username) {
		return new StaticTestPreferences(PREFERENCES_PREFIX, username);
	}

	protected void setValue(String name, String value) {
		values.put(name, value);
	}

	protected void waitForValue(final String name, int time) {
		ConditionWaiter.wait(new ConditionWaiter.Condition() {
			@Override
			public boolean test() {
				return values.get(name) != null;
			}
		}, time);
	}

	public WClient getClient(final boolean bind) throws MalformedURLException, SAXException {
		String username = getRandomUserName();
		WPreferences p = new StaticTestPreferences("waazdohclienttests", username);

		// p.set(IPFSServiceClient.IPFS_LOCALPATH, p.get(WPreferences.LOCAL_PATH, "") +
		// "ipfs");
		p.set(IPFSServiceClient.IPFS_LOCALPATH, System.getProperty("user.home") + "/.ipfs");

		new File(p.get(IPFSServiceClient.IPFS_LOCALPATH, "")).mkdirs();

		WTimedFlag ssbflag = new WTimedFlag(120000);
		WTimedFlag ssbsuccessflag = new WTimedFlag(120000);

		new Thread(() -> {
			try {
				String apiurl = runSsb();
				log.info("runSsb apiurl " + apiurl);
				if (apiurl != null) {
					p.set(IPFSServiceClient.API_URL, apiurl);
					ssbsuccessflag.trigger();
				}
			} catch (IOException ssbe) {
				log.error(ssbe);
			} finally {
				ssbflag.trigger();
			}
		}).start();

		if (ipfsclients.size() <= usernamecounter) {
			log.info("waiting for ssb api");
			ssbflag.waitTimer();
			if (!ssbsuccessflag.isTriggered()) {
				log.info("ssb not success");
				return null;
			}
			log.info("api running");

			IPFSServiceClient ipfs = new IPFSServiceClient(p);

			try {
				for (IPFSServiceClient ipfsclient : ipfsclients) {
					ipfs.connect(ipfsclient.getAddress());
				}
			} catch (IOException e) {
				log.error(e);
			}

			ipfsclients.add(ipfs);
		}

		WClient c = new WClient(p, ipfsclients.get(usernamecounter));

		usernamecounter++;

		return c;
	}

	protected String getRandomUserName() {
		return "username" + (usernamecounter) + "_" + System.currentTimeMillis();
	}

	public String getTempPath() {
		String tempDir = System.getProperty("java.io.tmpdir") + File.separator + "user_"
				+ System.getProperty("user.name");
		return tempDir;
	}

	private void readStream(String name, Process p, InputStream is) {
		new Thread(() -> {
			BufferedReader bis = new BufferedReader(new InputStreamReader(is, Charset.defaultCharset()));
			try {
				while (p.isAlive() && !stopped) {
					String readLine = bis.readLine();
					if (readLine != null) {
						log.info("TestEnv " + name + " : " + readLine);
					}
				}
			} catch (IOException e) {
				log.error(e);
			}
		}).start();
	}

	public void end() {

		stopped = true;

		for (SSBProcesses p : ssbprocesses) {
			// p.destroy();
			log.info("still running " + p);
		}

		usernamecounter = 0;

		log.info("************************* TEARDOWN DONE *****");
	}

	public String getValue(Object valuename) {
		return values.get(valuename);
	}

	class SSBProcesses {
		Process api;
	}
}
