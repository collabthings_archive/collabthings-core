/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen
 ******************************************************************************/
package org.collabthings.core.utils;

public class ConditionWaiter {
	private boolean done;
	private int maxtime;
	private Condition c;
	private int delay = 100;

	private ConditionWaiter(final Condition c, final int nmaxtime) {
		this.c = c;
		this.maxtime = nmaxtime;
		if (maxtime <= 0) {
			maxtime = Integer.MAX_VALUE;
		}
	}

	private synchronized void loop() throws InterruptedException {
		long st = System.currentTimeMillis();

		while ((System.currentTimeMillis() - st) < maxtime && !c.test()) {
			this.wait(delay);
		}

		done = true;
	}

	public boolean isDone() {
		return done;
	}

	@FunctionalInterface
	public static interface Condition {
		boolean test();
	}

	public static ConditionWaiter wait(Condition c, int delay, int timeout) {
		ConditionWaiter waiter = new ConditionWaiter(c, timeout);
		waiter.delay = delay;
		try {
			waiter.loop();
		} catch (InterruptedException e) {
			WLogger.getLogger(waiter).error(e);
			Thread.currentThread().interrupt();
		}

		return waiter;
	}

	public static ConditionWaiter wait(Condition c, int timeout) {
		ConditionWaiter waiter = new ConditionWaiter(c, timeout);
		try {
			waiter.loop();
		} catch (InterruptedException e) {
			WLogger.getLogger(waiter).error(e);
			Thread.currentThread().interrupt();
		}

		return waiter;
	}
}
