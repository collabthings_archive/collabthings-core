/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen
 ******************************************************************************/
package org.collabthings.core.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

public final class StaticTestPreferences extends PropertiesPreferences {
	private static List<Integer> ports = new ArrayList<Integer>();

	public StaticTestPreferences(final String prefix, final String username) {
		super(prefix + "/" + username);
		//
		Preferences prefs = getPrefs();
		if ("".equals(prefs.get(WPreferences.SERVICE_URL, ""))) {
			prefs.put(WPreferences.SERVICE_URL, "http://10.0.1.30:18099/waazdoh");
		}

		String deflocalpath = "";
		if (new File("target").exists()) {
			deflocalpath = "target" + File.separator;
		}
		deflocalpath = System.getProperty("user.home") + "/.waazdohclienttest" + File.separator + username
				+ File.separator;

		String lpath = prefs.get(WPreferences.LOCAL_PATH, "");
		if ("".equals(lpath)) {
			lpath = deflocalpath;
			prefs.put(WPreferences.LOCAL_PATH, lpath);
		}

		WLogger.getLogger(this).info("Local path " + lpath);

		// creating a random port
		int port = randomPort();
		getPrefs().putInt(WPreferences.NETWORK_SERVER_PORT, port);
		getPrefs().putInt(WPreferences.NETWORK_SERVER_DEFAULT_PORT, port);
		ports.add(port);

		prefs.putInt(WPreferences.NETWORK_MAX_DOWNLOADS, 8);
	}

	@Override
	public String get(String name) {
		return get(name, "");
	}

	@Override
	public String get(String name, String defaultvalue) {
		if (name.equals(SERVERLIST)) {
			int port = getPrefs().getInt(NETWORK_SERVER_PORT, 19000);
			// creating a list of servers used in other clients in this test.
			String serverlist = "";
			for (Integer serviceport : ports) {
				if (serviceport != port) {
					serverlist += "localhost:" + serviceport + ",";
				}
			}

			getPrefs().put(WPreferences.SERVERLIST, serverlist);
		}

		return super.get(name, defaultvalue);
	}

	public static void clearPorts() {
		ports = new ArrayList<Integer>();
	}

	private int randomPort() {
		int port = 9000 + (int) (Math.random() * 10000);
		return port;
	}
}
