/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen - initial API and implementation
 ******************************************************************************/
package org.collabthings.core.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class WLogger {
	private static long starttime;
	private final Logger log;
	private Object o;

	static {
		WLogger.starttime = System.currentTimeMillis();
	}

	public WLogger(Object o) {
		this.o = o;
		log = LoggerFactory.getLogger(o.getClass().getName());
	}

	public static WLogger getLogger(Object o) {
		WLogger logger = new WLogger(o);
		return logger;
	}

	public void info(final String string) {
		getLog().info(getLine(string));
	}

	public void debug(final String string) {
		getLog().debug(getLine(string));
	}

	private String getLine(final String message) {
		StringBuilder sb = new StringBuilder();

		sb.append("" + o);
		sb.append(" - ");
		sb.append(System.currentTimeMillis() - WLogger.starttime);
		sb.append(" -- ");
		if (message != null) {
			sb.append(message.replace('\n', '-'));
		} else {
			sb.append("null message");
		}
		return sb.toString();
	}

	private Logger getLog() {
		return log;
	}

	public void error(Throwable cause) {
		getLog().error(cause.toString());
		getLog().error("", cause);
	}

	public void error(final String string) {
		info("ERROR " + string);
	}

	public static void resetStartTime() {
		WLogger.starttime = System.currentTimeMillis();
	}

	public void warning(String string) {
		getLog().warn(string);
	}
}
