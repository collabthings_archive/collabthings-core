/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen
 ******************************************************************************/
package org.collabthings.core.utils;

import java.util.HashSet;
import java.util.Set;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

public class PropertiesPreferences implements WPreferences {
	private final String prefix;

	public PropertiesPreferences(String prefix) {
		this.prefix = prefix;
	}

	@Override
	public Set<String> getNames() {
		Set<String> ret = new HashSet<String>();
		String[] keys;
		try {
			keys = getPrefs().keys();
			for (final String string : keys) {
				ret.add(string);
			}

			return ret;
		} catch (BackingStoreException e) {
			WLogger.getLogger(this).error(e);
			return new HashSet<String>();
		}
	}

	public Preferences getPrefs() {
		Preferences prefs = Preferences.userNodeForPackage(getClass()).node(prefix);
		return prefs;
	}

	@Override
	public void set(final String name, String value) {
		getPrefs().put(name, value);
	}

	@Override
	public void set(final String name, boolean b) {
		set(name, "" + b);
	}

	@Override
	public int getInteger(final String string, int i) {
		String sint = get(string, "" + i);
		return Integer.parseInt(sint);
	}

	@Override
	public boolean getBoolean(final String name, boolean defbool) {
		return "true".equals(get(name, "" + defbool));
	}

	public String get(String name) {
		return get(name, "");
	}

	@Override
	public String get(final String name, String defaultvalue) {
		String get = getValue(name);
		if (get == null || "".equals(get)) {
			Object systemproperty = System.getProperties().get(name);
			if (systemproperty != null) {
				defaultvalue = "" + systemproperty;
			}
			getPrefs().put(name, defaultvalue);
			return defaultvalue;
		} else {
			return get;
		}
	}

	private String getValue(final String string) {
		return getPrefs().get(string, "");
	}

	@Override
	public double getDouble(String string, double d) {
		String sdouble = get(string, "" + d);
		return Double.parseDouble(sdouble);
	}
}
