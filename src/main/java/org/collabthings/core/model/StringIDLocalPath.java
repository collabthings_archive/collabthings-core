/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen
 ******************************************************************************/
package org.collabthings.core.model;

import java.io.File;

import org.collabthings.datamodel.WStringID;

public class StringIDLocalPath {

	private String localpath;
	private WStringID id;

	public StringIDLocalPath(String path, WStringID nid) {
		this.localpath = path;
		this.id = nid;
	}

	public String getPath() {
		String spath = "" + localpath;
		String sid = id.toString();
		// creating a directory tree
		// first four characters are likely a year.
		
		int index = 0;
		int indexjump = 4;
		while (index <= 4) {
			spath += File.separatorChar;
			spath += sid.substring(index, index + indexjump);
			index += indexjump;
			if (indexjump > 2) {
				indexjump = 2;
			}
		}

		spath += File.separatorChar;
		spath = spath.replace("" + File.separatorChar + File.separatorChar, ""
				+ File.separatorChar);
		return spath;
	}
}
