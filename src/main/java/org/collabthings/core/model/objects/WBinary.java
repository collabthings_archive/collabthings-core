/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen - initial API and implementation
 ******************************************************************************/
package org.collabthings.core.model.objects;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

import org.collabthings.core.BinarySource;
import org.collabthings.core.ServiceObject;
import org.collabthings.core.ServiceObjectData;
import org.collabthings.core.WServiceClient;
import org.collabthings.core.utils.WLogger;
import org.collabthings.datamodel.WHashSource;
import org.collabthings.datamodel.WObject;
import org.collabthings.datamodel.WStringID;
import org.collabthings.datamodel.WUserID;
import org.collabthings.datamodel.WaazdohInfo;

public final class WBinary implements WHashSource, ServiceObjectData {
	public static final String BEAN_TAG = "binary";
	//
	private long length = -1;
	private long timestamp;
	//
	private WLogger log = WLogger.getLogger(this);
	private List<BinaryListener> listeners;
	//
	private ServiceObject so;

	private WUserID creatorid;
	private String version;
	private String comment = "";
	private long usedtime;
	private String extension;
	// private WStringID id = new WStringID();
	private RandomAccessFile access;
	private String storage;

	private String ipfsid;
	private boolean changed;
	private BinarySource binarysource;
	private WServiceClient client;
	private WUserID userid;
	private boolean ready;

	private ArrayList<Byte> bb = new ArrayList<>();

	private static int binarycount = 0;

	public WBinary(WServiceClient client, String storagepath, String comment, String extension) {
		this.storage = storagepath;
		this.client = client;

		this.userid = new WUserID(client.getUser().getUserid());

		if (userid != null) {
			this.creatorid = userid;
		}

		if (extension == null || extension.length() == 0) {
			this.extension = "bin";
		}

		so = new ServiceObject(BEAN_TAG, userid, client.getObjects(), this, WaazdohInfo.VERSION, extension);

		version = WaazdohInfo.VERSION;
		this.extension = extension;
		this.comment = comment;

		binarycount++;

		used();
	}

	public boolean load(WStringID sid) {
		return so.load(sid);
	}

	@Override
	public String getHash() {
		return so.getHash();
	}

	private void init() {
		setChanged();
	}

	public synchronized boolean load(InputStream is) {
		try {
			newFile();

			log.info("" + this + " loading from inputstream " + is);

			byte[] nbytes = new byte[1000];
			// @
			while (true) {
				int count = is.read(nbytes);
				if (count <= 0) {
					break;
				}
				add(nbytes, count);
			}

			return true;
		} catch (IOException e) {
			log.error(e);
			return false;
		}
	}

	private synchronized void newFile() {
		log.info("new file");
		bb = new ArrayList<>();

		changed = true;
	}

	public synchronized void addAt(int index, byte[] nbytes) {
		for (int i = 0; i < nbytes.length; i++) {
			bb.set(index++, nbytes[i]);
		}
		//
		log.debug("added " + nbytes.length + " at " + index + ". File size now " + bb.size());
		setChanged();
	}

	public synchronized void addAt(int index, byte[] nbytes, int length) {
		for (int i = 0; i < length; i++) {
			bb.set(index++, nbytes[i]);
		}
		//
		debugAdded(length, index, bb.size());
		setChanged();
	}

	public synchronized void add(byte[] nbytes, int length) {
		used();

		for (int i = 0; i < length; i++) {
			bb.add(nbytes[i]);
		}

		debugAdded(nbytes.length, 0, bb.size());

		setChanged();
	}

	public synchronized void add(byte[] bytes) {
		used();

		for (int i = 0; i < bytes.length; i++) {
			bb.add(bytes[i]);
		}

		debugAdded(bytes.length, 0, bb.size());
		setChanged();
	}

	private void debugAdded(int length, int index, long l) {
		log.debug("added " + length + " at " + index + ". File size now " + l);
	}

	public synchronized void add(Byte b) {
		bb.add(b);
		setChanged();
	}

	public synchronized boolean read(int start, byte[] bs) {
		if (isReady()) {
			for (int i = 0; i < bs.length; i++) {
				bs[i] = bb.get(start + i);
			}
			return true;
		} else {
			return false;
		}
	}

	public synchronized boolean publish() {
		if (save()) {
			so.publish();
			String path = "binaries/" + getExtension() + "/" + System.currentTimeMillis();
			String data = so.getID().toString();
			client.getStorageArea().write(path, data);
			return true;
		} else {
			return false;
		}
	}

	public synchronized boolean save() {
		try {
			creatorid = userid;

			ipfsid = client.getBinarySource().writeData(getInputStream());

			so.save();
			changed = false;

			return true;
		} catch (IOException e) {
			log.error(e);
			return false;
		}
	}

	@Override
	public String toString() {
		return "Binary[" + ipfsid + "][no:" + binarycount + "][" + length() + "][" + comment + "]";
	}

	@Override
	public boolean parse(WObject d) {
		this.length = d.getIntValue("length");
		this.ipfsid = d.getValue("ipfsid");
		this.creatorid = new WUserID(d.getValue("creator"));
		this.version = d.getValue("version");
		this.extension = d.getValue("extension");
		this.comment = d.getValue("comment");

		new Thread(() -> {
			byte[] bs = client.getBinarySource().readData(ipfsid);
			bb = new ArrayList<>();
			add(bs);
			setReady();
		}, "load binary " + ipfsid).start();

		return true;
	}

	@Override
	public synchronized WObject getObject() {
		WObject b = so.getBean();
		//
		b.addValue("length", Long.toString(length));

		b.addValue("creator", "" + creatorid);
		b.addValue("version", version);
		b.addValue("comment", "" + comment);
		b.addValue("extension", extension);
		b.addValue("ipfsid", "" + ipfsid);
		return b;
	}

	public long length() {
		return length;
	}

	private void used() {
		this.usedtime = System.currentTimeMillis();
	}

	public synchronized InputStream getInputStream() throws FileNotFoundException {
		byte[] bs = getContent();
		return new BufferedInputStream(new ByteArrayInputStream(bs));
	}

	public byte[] getContent() {
		byte[] barray = new byte[bb.size()];
		for (int i = 0; i < barray.length; i++) {
			barray[i] = bb.get(i);
		}
		return barray;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public synchronized void setChanged() {
		changed = true;
		timestamp = System.currentTimeMillis();
	}

	public synchronized boolean isReady() {
		return ready && !changed;
	}

	// @Override
	// public int hashCode() {
	// return id.hashCode();
	// }

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof WBinary) {
			WBinary bin = (WBinary) obj;
			if (!bin.getIpfsID().equals(getIpfsID())) {
				return false;
			}
			return true;
		}
		return false;
	}

	private String getIpfsID() {
		return ipfsid;
	}

	public synchronized void setReady() {
		log.info("setting binary ready");

		used();
		ready = true;
		changed = false;
		length = bb.size();

		fireReady();
	}

	private void fireReady() {
		if (listeners != null) {
			List<BinaryListener> ls = this.listeners;
			for (final BinaryListener listener : ls) {
				log.info("notifying READY " + listener);
				Thread t = new Thread((Runnable) () -> listener.ready(this), "Binary FireReady");
				t.start();
			}
		}
	}

	public void addListener(BinaryListener listener) {
		getListeners().add(listener);
	}

	private List<BinaryListener> getListeners() {
		if (listeners == null) {
			listeners = new ArrayList<BinaryListener>();
		}
		return listeners;
	}

	public void clear() {
		init();
	}

	public boolean isUsed(int maxdtime) {
		if (isReady()) {
			long dtime = System.currentTimeMillis() - usedtime;
			boolean ret = dtime < maxdtime;
			log.info("isused " + ret + " " + dtime);
			return ret;
		} else {
			return true;
		}
	}

	public String getExtension() {
		return extension;
	}

	public synchronized void importStream(InputStream stream) throws IOException {
		BufferedInputStream bis = new BufferedInputStream(stream);
		byte[] bs = new byte[1024];
		while (true) {
			int read = bis.read(bs);
			if (read < 0) {
				break;
			}
			add(bs, read);
		}
		bis.close();
		stream.close();
		setReady();
	}

	public synchronized void flush() {
		setChanged();
	}

	public WStringID getID() {
		return so.getID().getStringID();
	}

	/*
	 * public WObject getBean() { return so.getBean(); }
	 */

	public WServiceClient getService() {
		return client;
	}
}
