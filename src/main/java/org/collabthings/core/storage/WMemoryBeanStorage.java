/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen
 ******************************************************************************/
package org.collabthings.core.storage;

import java.util.HashMap;
import java.util.Map;

import org.collabthings.core.BeanStorage;
import org.collabthings.datamodel.WObject;
import org.collabthings.datamodel.WStringID;

public class WMemoryBeanStorage implements BeanStorage {

	private Map<WStringID, WObject> beans = new HashMap<WStringID, WObject>();

	@Override
	public void addObject(final WStringID id, WObject response) {
		beans.put(id, response);
	}

	@Override
	public WObject getBean(final WStringID id) {
		return beans.get(id);
	}

	@Override
	public Iterable<WStringID> getLocalSetIDs(String search) {
		return beans.keySet();
	}

	@Override
	public void remove(WStringID id) {
		beans.remove(id);
	}
}
