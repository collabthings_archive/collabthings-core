/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen - initial API and implementation
 ******************************************************************************/
package org.collabthings.core.storage.local;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.collabthings.core.BeanStorage;
import org.collabthings.core.model.StringIDLocalPath;
import org.collabthings.core.utils.WLogger;
import org.collabthings.core.utils.WPreferences;
import org.collabthings.datamodel.WObject;
import org.collabthings.datamodel.WStringID;
import org.collabthings.datamodel.WaazdohInfo;

public final class FileBeanStorage implements BeanStorage {
	private WLogger log = WLogger.getLogger(this);
	private String path;

	public FileBeanStorage(WPreferences preferences) {
		this.path = preferences.get(WPreferences.LOCAL_PATH, WPreferences.LOCAL_PATH_DEFAULT) + File.separator
				+ "beans";
		File file = new File(path);
		file.mkdirs();
	}

	public WObject getBean(final WStringID id) {
		try {
			File f = getFile(id);
			if (f.exists()) {
				String content = new String(Files.readAllBytes(Paths.get(f.getAbsolutePath())), WaazdohInfo.CHARSET);
				WObject o = new WObject();
				o.parse(content);
				return o;
			} else {
				return null;
			}
		} catch (IOException e) {
			log.error(e);
			return null;
		}
	}

	private File getFile(final WStringID id) {
		return new File(getFilePath(id));
	}

	private String getFilePath(final WStringID id) {
		String filepath = new StringIDLocalPath(this.path, id).getPath();
		File fpath = new File(filepath);
		if (!fpath.isDirectory()) {
			fpath.mkdirs();
		}
		//
		return filepath + id + ".yml";
	}

	@Override
	public void remove(WStringID id) {
		File f = getFile(id);
		if (f.exists() && !f.delete()) {
			log.error("deleting file " + f + " failed");
		}
	}

	public void addObject(final WStringID id, WObject response) {
		try {
			File f = getFile(id);
			FileUtils.writeStringToFile(f, response.toYaml(), WaazdohInfo.CHARSET);
		} catch (IOException e) {
			log.error(e);
		}
	}

	@Override
	public Iterable<WStringID> getLocalSetIDs(final String search) {
		final Iterator<File> fileiterator = getFileItarator(search);
		Iterable<WStringID> ids = getStringIDIterator(fileiterator);
		return ids;
	}

	private Iterable<WStringID> getStringIDIterator(final Iterator<File> fileiterator) {
		return () -> new Iterator<WStringID>() {
			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}

			@Override
			public boolean hasNext() {
				return fileiterator.hasNext();
			}

			@Override
			public WStringID next() {
				File f = fileiterator.next();
				String name = f.getName();
				return new WStringID(name.replace(".yml", ""));
			}
		};
	}

	private Iterator<File> getFileItarator(final String search) {
		return FileUtils.iterateFiles(new File(path), new IOFileFilter() {

			@Override
			public boolean accept(File f) {
				String abspath = f.getAbsolutePath().replace(File.separator, "");
				boolean b = abspath.indexOf(search) >= 0;
				
				if(b) {
					log.info("fileiterator accept " + f);
				}
				
				return b;
			}

			@Override
			public boolean accept(File arg0, String arg1) {
				log.info("not accepting " + arg0);

				return false;
			}

		}, new IOFileFilter() {

			@Override
			public boolean accept(File arg0, String arg1) {
				return true;
			}

			@Override
			public boolean accept(File arg0) {
				return true;
			}
		});
	}

}
