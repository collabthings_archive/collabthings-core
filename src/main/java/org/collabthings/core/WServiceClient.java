/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen
 ******************************************************************************/
package org.collabthings.core;

import org.collabthings.common.service.ObjectsService;
import org.collabthings.common.service.StorageAreaService;
import org.collabthings.common.service.UsersService;
import org.collabthings.datamodel.UserVO;

public interface WServiceClient {

	StorageAreaService getStorageArea();

	UsersService getUsers();

	ObjectsService getObjects();

	UserVO getUser();

	UserVO getUser(String username);

	BinarySource getBinarySource();

	void close();

}
