/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen - initial API and implementation
 ******************************************************************************/
package org.collabthings.common.service;

import java.util.List;

import org.collabthings.datamodel.ProfileVO;
import org.collabthings.datamodel.ReturnVO;
import org.collabthings.datamodel.UserVO;

public interface UsersService {
	ProfileVO getProfile();

	UserVO getUser(String userid);

	UserVO getWithName(String username);

	List<UserVO> search(String string, int i);

	List<UserVO> newUsers(int count);

	ReturnVO saveProfile(ProfileVO profile);

	void follow(String userid);

	List<String> getFollowing();

}
