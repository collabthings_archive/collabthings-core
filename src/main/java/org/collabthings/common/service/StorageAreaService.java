/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen - initial API and implementation
 ******************************************************************************/
package org.collabthings.common.service;

import java.util.List;
import java.util.Map;

public interface StorageAreaService {

	Map<String, String> getList(String path);

	String read(String path);

	String read(String userid, String string);

	List<String> listNewItems(String userid, int i, int j);

	void write(String path, String data);

}
