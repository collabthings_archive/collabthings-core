/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen - initial API and implementation
 ******************************************************************************/
package org.collabthings.common.service;

import java.util.List;

import org.collabthings.datamodel.MessageVO;

public interface MessagesService {

	boolean deleteMessage(String id);

	MessageVO getMessage(String id);

	List<MessageVO> getMessages(int i, String type);

	boolean sendMessage(String userid, String string, String string2);

}
