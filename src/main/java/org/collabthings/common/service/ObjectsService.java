/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen - initial API and implementation
 ******************************************************************************/
package org.collabthings.common.service;

import java.util.List;

import org.collabthings.core.BeanStorage;
import org.collabthings.core.ServiceObject;
import org.collabthings.core.WClient.Filter;
import org.collabthings.datamodel.ObjectVO;
import org.collabthings.datamodel.ReturnVO;
import org.collabthings.datamodel.WObject;

public interface ObjectsService {
	public int ERROR_DEFAULT = 1;

	ObjectVO read(String string);

	List<ObjectVO> search(String search, int i, int j);

	ObjectVO write(String objectid, String testdata);

	ReturnVO publish(String objectid);

	ReturnVO error(String id, String message);

	void addBeanStorage(BeanStorage bstorage);

	void addFilter(Filter f);

	boolean filter(WObject wo);

}
