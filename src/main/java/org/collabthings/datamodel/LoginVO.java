/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen
 ******************************************************************************/
package org.collabthings.datamodel;

public class LoginVO extends ReturnVO {
	private String userid;
	private String sessionid;
	private String username;
	private String accesstoken;
	private String oauthid;
	private String service;

	@Override
	public String toString() {
		return "LoginVO[" + userid + "][" + sessionid + "][" + username + "][" + service + "][" + accesstoken + "]["
				+ oauthid + "]";
	}

	public String getSessionid() {
		return sessionid;
	}

	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAccesstoken() {
		return this.accesstoken;
	}

	public void setAccesstoken(String accesstoken) {
		this.accesstoken = accesstoken;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getOauthid() {
		return oauthid;
	}

	public void setOauthid(String oauthid) {
		this.oauthid = oauthid;
	}

}
