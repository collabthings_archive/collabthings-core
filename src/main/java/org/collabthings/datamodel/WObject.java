package org.collabthings.datamodel;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.codec.binary.Base64;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.DumperOptions.FlowStyle;
import org.yaml.snakeyaml.Yaml;

public class WObject {
	private static final String UNKNOWN = "unknown";
	private static final String ATTR_TYPE = "type";

	private static final List<String> hashignoredkeys = new ArrayList<>();
	private StringBuilder tmp = new StringBuilder();

	static {
		hashignoredkeys.add("id");
		hashignoredkeys.add("modified");
		hashignoredkeys.add("version");
		hashignoredkeys.add("creator");
	}

	private Map<String, Object> map = new HashMap<String, Object>();

	public WObject(String name) {
		setAttribute(ATTR_TYPE, name);
	}

	public WObject() {
		// setAttribute(ATTR_TYPE, UNKNOWN);
	}

	public WObject(Map<String, Object> child) {
		if (child != null) {
			this.map = child;
		}
	}

	public void parse(String string) {
		if (string != null) {
			Yaml y = new Yaml();
			map = (Map<String, Object>) y.load(new StringReader(string));
		}
	}

	public String getType() {
		return getAttribute(ATTR_TYPE);
	}

	public synchronized void setAttribute(String string, String value) {
		map.put(string, value);
	}

	public synchronized String getAttribute(String string) {
		return (String) map.get(string);
	}

	public WUserID getUserAttribute(String string) {
		String a = getAttribute(string);
		if (a != null) {
			return new WUserID(a);
		} else {
			return null;
		}
	}

	public long getLongValue(String string) {
		String a = getValue(string);
		if (a != null) {
			return Long.parseLong(a);
		} else {
			return 0;
		}
	}

	public int getIntValue(String string) {
		String a = getValue(string);
		if (a != null) {
			return Integer.parseInt(a);
		} else {
			return 0;
		}
	}

	public double getDoubleValue(String string) {
		String a = getValue(string);
		if (a != null) {
			return Double.parseDouble(a);
		} else {
			return 0;
		}
	}

	public String getValue(String string) {
		Map<String, Object> values = getValues();
		return (String) values.get(string);
	}

	private Map<String, Object> getValues() {
		return map;
	}

	public void addValue(String string, String string2) {
		if (string == null) {
			throw new RuntimeException("addValue name cannot be null");
		}

		Map<String, Object> values = getValues();
		values.put(string, string2);
	}

	public WStringID getIDValue(String string) {
		String sid = getValue(string);
		if (sid != null) {
			return new WStringID(sid);
		} else {
			return null;
		}
	}

	public String toYaml() {
		DumperOptions o = new DumperOptions();
		o.setDefaultFlowStyle(FlowStyle.BLOCK);
		Yaml y = new Yaml(o);
		return y.dump(this.map);
	}

	public String toText() {
		tmp.setLength(0);
		toText(tmp, map);

		return tmp.toString();
	}

	private void toText(StringBuilder sb, Map<String, Object> map) {
		for (String key : map.keySet()) {
			sb.append("\n");
			sb.append(key);
			sb.append("->");
			Object o = map.get(key);
			if (o != null) {
				if (o instanceof Map) {
					Map<String, Object> childmap = (Map) o;
					toText(sb, childmap);
				} else {
					sb.append(o.toString());
				}
			} else {
				sb.append("null");
			}
		}
	}

	@Override
	public String toString() {
		return toText();
	}

	@Override
	public int hashCode() {
		return toText().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof WObject) {
			WObject o = (WObject) obj;
			return o.toYaml().equals(toYaml());
		} else if (obj instanceof String) {
			String s = (String) obj;
			return s.equals(toYaml());
		} else {
			return false;
		}
	}

	public String getContentHash() {
		HashMap<String, Object> contentmap = new HashMap<String, Object>();

		WObject o;

		synchronized (this) {
			if (map.get("content") != null) {
				o = get("content");
			} else {
				for (String key : map.keySet()) {
					if (!key.equals("id")) {
						contentmap.put(key, map.get(key));
					}
				}
				o = new WObject(contentmap);
			}
		}

		return new WBytesHash(o.toText().getBytes()).toString();
	}

	public void addValue(String string, int i) {
		addValue(string, "" + i);
	}

	public void addValue(String string, long l) {
		addValue(string, "" + l);
	}

	public void addValue(String string, double d) {
		addValue(string, "" + d);
	}

	public void addValue(String name, WObjectID id) {
		addValue(name, id.getStringID().toString());
	}

	public void setBase64Value(final String valuename, String value) {
		if (value != null) {
			byte[] bytes64 = Base64.encodeBase64(value.getBytes());
			addValue(valuename, new String(bytes64));
		} else {
			delete(valuename);
		}
	}

	public String getBase64Value(final String string) {
		String value64 = getValue(string);
		if (value64 != null) {
			byte[] bytes = Base64.decodeBase64(value64.getBytes());
			return new String(bytes);
		} else {
			return null;
		}
	}

	private void delete(final String string) {
		getValues().remove(string);
	}

	public WObject get(String string) {
		Map<String, Object> child = (Map<String, Object>) getValues().get(string);
		if (child != null) {
			return new WObject(child);
		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<WObject> getObjectList(String listname) {
		Object o = getList(listname);

		List<WObject> ret = new ArrayList<WObject>();
		if (o != null) {
			List<Map<String, Object>> list = (List) o;

			for (Map<String, Object> listmap : list) {
				ret.add(new WObject(listmap));
			}
		}

		return ret;
	}

	public synchronized List getList(String listname) {
		Object object = map.get(listname);

		if (object == null) {
			object = new ArrayList();
			map.put(listname, object);
		}

		if (object instanceof List) {
			return (List) object;
		} else {
			return null;
		}
	}

	public synchronized void addToList(String listname, WObject o) {
		getObjectList(listname);
		List<Map<String, Object>> list = (List<Map<String, Object>>) map.get(listname);
		list.add(o.map);
	}

	public synchronized void add(String name, WObject o) {
		map.put(name, o.map);
	}

	public void addToList(String listname, String string) {
		List<String> list = (List<String>) getList(listname);
		list.add(string);
	}

	public synchronized WObject add(String name) {
		WObject o = new WObject();
		map.put(name, o.map);
		return o;
	}

	public synchronized Set<String> getChildren() {
		return map.keySet();
	}

	public void parseMap(String paramname, ParamParser e) {
		List<WObject> pbeans = getObjectList(paramname);
		for (WObject b : pbeans) {
			String param = b.getValue("name");
			String value = b.getValue("value");
			e.add(param, value);
		}
	}

	public void add(String paramname, Map<String, String> parameters) {
		getObjectList(paramname);

		Set<String> keys = parameters.keySet();
		for (String key : keys) {
			WObject c = new WObject();
			c.addValue("name", key);
			c.addValue("value", parameters.get(key));
			addToList(paramname, c);
		}
	}

	public interface ParamParser {
		void add(String name, String value);
	}

}
