/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen - initial API and implementation
 ******************************************************************************/
package org.collabthings.datamodel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.UUID;

public final class WObjectID {
	private final String id;
	private String storedhash = "unknown";
	private WHashSource hashsource;

	public WObjectID(final String ovalue, final WHashSource hsource) {
		String value = ovalue.replace(".", "_");
		StringTokenizer st = new StringTokenizer(value, "_");

		String sid = st.nextToken();
		if (st.hasMoreTokens()) {
			sid += "_" + st.nextToken();
		}

		WObjectID.check(sid);
		id = sid;

		this.hashsource = hsource;
	}

	public WObjectID(WHashSource hsource, final String nprefix) {
		Date date = Calendar.getInstance().getTime();
		SimpleDateFormat d = new SimpleDateFormat("yyyyMM");

		id = d.format(date) + nprefix + "_" + UUID.randomUUID().toString();
		hashsource = hsource;
	}

	public WObjectID(WStringID oid, WHashSource nhashsource) {
		this(oid.toString(), nhashsource);
	}

	public void setStoredhash(String nstoredhash) {
		this.storedhash = nstoredhash;
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	private String getSourceHash() {
		return hashsource.getHash();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof WObjectID) {
			WObjectID bid = (WObjectID) obj;
			return bid.id.equals(id) && getSourceHash().equals(bid.hashsource.getHash());
		} else if (obj instanceof WStringID) {
			return getStringID().equals((WStringID) obj);
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		String sid = id + "_" + getSourceHash();
		if (storedhash != null) {
			sid += "_" + storedhash;
		}

		return sid;
	}

	public String getContentId() {
		return id + "_" + getSourceHash();
	}

	public String getBase() {
		return id;
	}

	public static void check(final String substring) {
		String[] split = substring.split("_");
		String prefixpart = split[0];
		if (prefixpart.length() < 8) {
			throw new IllegalArgumentException(
					"prefix value length " + prefixpart.length() + ". Should at least three characters.");
		}

		if (split.length > 1) {
			String idpart = split[1];
			if (idpart.length() != 36) {
				throw new IllegalArgumentException("id value length " + idpart.length());
			}
		}
	}

	public WStringID getStringID() {
		return new WStringID(toString());
	}

	public boolean isID(WStringID oid) {
		return getStringID().equals(oid);
	}

}
