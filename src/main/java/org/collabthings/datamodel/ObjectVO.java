/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen
 ******************************************************************************/
package org.collabthings.datamodel;

import java.io.Serializable;

public class ObjectVO extends ReturnVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String data;
	private String id;

	private String hash;

	public ObjectVO() {
		setSuccess(true);
	}

	public ObjectVO(WObject o) {
		this.setData(o.toYaml());
	}

	public ObjectVO(String data) {
		this.setData(data);
	}

	public ObjectVO(Error e) {
		super(e);
	}

	public WObject toObject() {
		WObject o = new WObject();
		o.parse(getData());
		return o;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setHash(String string) {
		this.hash = string;
	}

	public String getHash() {
		return hash;
	}

}
