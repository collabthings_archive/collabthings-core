package org.collabthings.datamodel;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.collabthings.core.utils.WLogger;

public final class WBytesHash {

	private byte[] hash;

	public WBytesHash(byte[] bytes) {
		calculate(bytes);
	}

	private void calculate(byte[] bytes) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-256");
			hash = md.digest(bytes);
		} catch (NoSuchAlgorithmException e) {
			WLogger.getLogger(this).error(e);

		}
	}

	public WBytesHash(InputStream inputStream) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte bs[] = new byte[1024];
		int read;
		while ((read = inputStream.read(bs)) >= 0) {
			baos.write(bs, 0, read);
		}

		calculate(baos.toByteArray());
		inputStream.close();
	}

	@Override
	public String toString() {
		BigInteger bigInteger = new BigInteger(hash);
		String s = String.format("%040x", bigInteger);
		return s.replace('-', 'A');
	}

}
