/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen
 ******************************************************************************/
package org.collabthings.datamodel;

import java.util.Locale;

public final class WaazdohInfo {
	public static final String VERSION = "1.1.4";
	public static final String CHARSET = "UTF-8";
	public static final Locale LOCALE = Locale.ENGLISH;
	public static final int DEFAULT_TIMEOUT = 30000;

	private static boolean devserver = false;

	private WaazdohInfo() {
	}

	public static boolean isDevServer() {
		return WaazdohInfo.devserver;
	}

	public static void setDevServer(boolean b) {
		WaazdohInfo.devserver = b;
	}
}
