/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen
 ******************************************************************************/
package org.collabthings.datamodel;

public class UserVO extends ReturnVO {
	private String username;
	private String userid;
	private String email;

	public UserVO(Error error) {
		super(error);
	}

	public UserVO() {
	}

	@Override
	public String toString() {
		return "UserVO[" + username + "][" + userid + "]";
	}

	public static UserVO getFalse() {
		UserVO vo = new UserVO();
		vo.setSuccess(false);
		return vo;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
