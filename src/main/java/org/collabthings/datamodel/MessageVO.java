/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen
 ******************************************************************************/
package org.collabthings.datamodel;

public class MessageVO extends ReturnVO {
	public static final String MESSAGEID = "messageid";
	public static final String TIMESTAMP = "timestamp";
	public static final String MESSAGE = "message";
	public static final String TYPE = "type";
	public static final String CREATORID = "creatorid";
	public static final String READ = "read";

	private String text;
	private String creatorid;
	private String type;
	private String id;

	public MessageVO(Error error) {
		super(error);
	}

	public MessageVO() {
		setSuccess(true);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCreatorid() {
		return creatorid;
	}

	public void setCreatorid(String creatorid) {
		this.creatorid = creatorid;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
