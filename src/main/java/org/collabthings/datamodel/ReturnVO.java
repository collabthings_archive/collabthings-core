/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen
 ******************************************************************************/
package org.collabthings.datamodel;

import java.util.HashMap;
import java.util.Map;

public class ReturnVO {
	private boolean success = true;
	private String message;
	private Map<String, String> values;
	private long timestamp = System.currentTimeMillis();

	public ReturnVO(Error error) {
		setMessage("" + error);
		success = false;
	}

	public ReturnVO() {
		success = true;
	}

	public ReturnVO(String string) {
		this.message = string;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public static ReturnVO getAuthenticationError() {
		ReturnVO vo = new ReturnVO(new Error("Authentication error"));
		return vo;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public static ReturnVO getTrue() {
		ReturnVO vo = new ReturnVO();
		vo.setSuccess(true);
		return vo;
	}

	public static ReturnVO getError(String string) {
		ReturnVO vo = new ReturnVO();
		vo.setSuccess(false);
		vo.setMessage(string);
		return vo;
	}

	public void addValue(String string, String string2) {
		this.getValues().put(string, string2);
	}

	public Map<String, String> getValues() {
		if (values == null) {
			setValues(new HashMap<String, String>());
		}
		return values;
	}

	public void setValues(Map<String, String> values) {
		this.values = values;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public static ReturnVO get(boolean b) {
		ReturnVO v = new ReturnVO();
		v.setSuccess(b);
		return v;
	}

	@Override
	public String toString() {
		return "ReturnVO[" + timestamp + "][" + message + "][" + values + "]";
	}
}
