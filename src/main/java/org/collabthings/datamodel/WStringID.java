package org.collabthings.datamodel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class WStringID {

	private String sid;

	public WStringID() {
		Date date = Calendar.getInstance().getTime();
		SimpleDateFormat d = new SimpleDateFormat("yyyyMM");
		sid = d.format(date) + UUID.randomUUID().toString();
	}

	public WStringID(final String sid) {
		this.sid = sid.toString();
	}

	@Override
	public String toString() {
		return sid;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		} else if (obj instanceof String) {
			return sid.equals((String) obj);
		} else if (obj instanceof WStringID) {
			WStringID bid = (WStringID) obj;
			return bid.sid.equals(sid);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return sid.hashCode();
	}

	public boolean isId() {
		if (sid == null) {
			return false;
		}

		if (sid.indexOf('-') < 0) {
			return false;
		}

		if (sid.length() < 30) {
			return false;
		}

		return true;
	}

}
