/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen
 ******************************************************************************/
package org.collabthings.datamodel;

public class StorageAreaVO extends ReturnVO {

	private String path;
	private String data;
	private String username;

	public StorageAreaVO(String path, String string) {
		this.path = path;
		this.data = string;
	}

	public StorageAreaVO() {
		//
	}

	public StorageAreaVO(String nusername, String npath, String nvalue) {
		this.username = nusername;
		this.path = npath;
		this.data = nvalue;
	}

	public StorageAreaVO(String string) {
		this.path = string;
	}

	public String getPath() {
		return path;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
